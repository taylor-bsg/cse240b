This repo contains the resources for Prof. Taylor's 
UCSD CSE 240B class, which includes a series of labs
that ramp up on the bsg_manycore system. Below are setup instructions.

** Setup **

A. First, get our Vivado/Linux VM environment, developed by Scott Davidson. Do you not have to use this, but we cannot provide support for any other environments.
 
https://drive.google.com/a/eng.ucsd.edu/folderview?id=0B0LXWenlFGdvcm9lb19kY0ZSTTg&usp=sharing
 
See the instructions for reassembling the pieces, etc.
 
B. Clone our CSE 240B git repo:
 
https://bitbucket.org/taylor-bsg/cse240b/src
 
(git clone https://bitbucket.org/taylor-bsg/cse240b.git should work, you may need provide an email address and username for git setup)
 
C. Verify that you can run Vivado by running our HW1, Vivado edition, in hw1 direction. See the makefile (run make setup first)
 
D. Build the RISC-V toolchain in riscv-tools:
 
    1. First, install necessary packages via make installs
    2. Second, checkout the sources via make checkout-all
    3. Third, build the baseline RISCV tools via make build-riscv-tools
    4. Finally, building the 32-bit tools via make build-riscv32
	
	
** Some hints **

1. Use the debug_p flag to enable tracing at various levels. Figure out how to tell when the processor is booted in the trace.
2. Use make <foo>.dis to disassemble the file <foo>.riscv.
3. See bsg_manycore/software/bsg_manycore_lib for helpful functions like bsg_finish() and bsg_print_time().
	